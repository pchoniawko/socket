import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import io from 'socket.io-client'

class App extends Component {

  constructor () {
    super()
    this.state = {
      messages: []
    }
  }
  componentDidMount () {
    this.socket = io.connect('http://localhost:5000')
    this.socket.on('message', message => {
      this.setState({ messages: [message, ...this.state.messages] })
    })
  }

  handleSubmit = event => {
    const body = event.target.value
    if (event.keyCode === 13 && body) {
      const message = {
        body,
        from: 'Me'
      }
      this.setState({ messages: [message, ...this.state.messages] })
      this.socket.emit('message', body)
      event.target.value = ''
    }
  }

  render() {
    const messages = this.state.messages.map((message, index) => {
      return <li key={index}>{message.from}: {message.body}</li>
    })
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React Real Time</h1>
        </header>
        <div><input type="text" onKeyUp={this.handleSubmit} /></div>
        <div>
          {messages}
        </div>
      </div>
    );
  }
}

export default App;
